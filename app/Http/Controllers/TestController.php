<?php

namespace App\Http\Controllers;

use App\Models\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    public function get_tests(Request $request) {
        $tests = Test::all();
        return view('test', ['tests' => $tests]);
    }

    public function create_test(Request $request) {
        $test = $request->all();
        // dd($test);
        Test::create([
            'user_id' => '1',
            'name' => $test["name"],
        ]);
        return redirect('/');
    }
}
